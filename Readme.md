# GAG_svFCS

GAG_svFCS implents a simulator of (sv)FCS experiments for HIV-1 Gag protein diffusion and binding at the plasma membrane, and a coupel of analysis tools. This is the code used for the following paper: 

Anita Mouttou, Erwan Bremaud, Rayane Dibsy, Coline Arone, Julien Noero, Johson Mak, Delphine Muriaux, Hugues
Berry, and Cyril Favard (2022) Quantifying membrane binding using Fluorescence Correlation Spectroscopy diffusion laws

*Usage*

The repository contains two main directories:

- FCS_Simulator: the files of the python/C++ (sv)FCS simulator
- FCS_Anlysis: matlab (.m) files for the analysis of the simulation results of the former code

## (1) (sv)FCS simulations

In the FCS_Simulator directory.

- the C++ code must first be compiled using :

g++ -g gag_sim.cpp jsoncpp.cpp Particle.cpp -o gag_sim -O2 -std=c++11

Once gag_sim is compiled:

- all the parameters can be set in the python file launch.py. 
Indicating vector values for parameters e.g., param1=[1 13 2], will launch one job per parameter value (e.g. one job for param1=1, one job for param1=13 and one job for param1=2). Beware of the combinatorics: param1=[1 3 5]; param2=[2 3 5 1 3], for instance, will launch 3x5= 15 jobs

- execute launch.py with:
python3.X launch.py

This will automatically launch all the specified jobs

- File output:
+ fcs_parameters.json saves all the parameters of the current execution of launch.py
+ output.log: a redirection of the standard output. Mostly tells the jobs that are finished (replicates)
+ log_time.json: the values of the delays tau used for the autocorrelation G(tau)
+ xxx_yyy_zzz_autocorrelation.json where xxx is the value of pon, yyy that of poff and zzz that of wxy: 
 the file that contains the autocorrelation function G(\tau) corresponding to each of the parameter sets indicated in launch.py. Warning: decimal separator (e.g. the dot .) is usually replaced by a "p" in the name of the autocorrelation.json files

## (2) Analysis of the FCS simulations
In the FCS_Analysis directory. The data to analyse are usually found in the data repository

The Fonctions directory contains a couple of fonctions used by the main analysis scripts:

- MapFits.m : Fits simulation results with adhoc autocorrelation functions. 
Execute it to compute Figure SI1 (data are given in 'data/MapSamplesDB1p7DF30')

- PhenomFitTrain.m: Fits phenomenological diffusion law on the training set with kon,koff, D2D and D3D known and the parameters of the diffusion law as free parameters.
Execute it to compute figures 1b1 and 1b2 (data are give in 'data/TrainSamples')

- PhenomFitTest.m: Fits the phenomenological diffusion law on the test set with kon,koff, D2D unknown and all the parameters of the diffusion law fixed by the training 
Execute it to compute figures 1c1, 1c2 and 1c3 (data are give in 'data/TestSamples')









