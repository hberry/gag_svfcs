% PhenomFitTrain.m - This script fits phenomenological diffusion law on the training set 
% with kon,koff, D2D and D3D known 
% and the parameters of the diffusion law as free parameters
% ie figure 1b1 and 1b2 of the article
%
%        
%AUTHOR: Hugues Berry
%         hugues.berry@inria.fr
%         http://www.inrialpes.fr/Berry/
%
%REFERENCE: Mouttou et al. Quantifying 2022 membrane binding using Fluorescence Correlation Spectroscopy diffusion laws
%
% LICENSE: CC0 1.0 Universal


close all;%clear all;
addpath 'Fonctions';
global dt Lnuc interaction_zone omega

%dt is the time step of the FCS simulator (in seconds)
%Lnuc is the linear size of the cubic nucleus (L in the article)
%interaction_zone is the distance below which a gag molecul can bind the
%membrane ($\epsilon$ un the article)
%omega is the ratio of w_z to w_xy (called s in the article)

reReadTSet=1;
%if reReadTSet=1, the code reads the raw simulation resuls from the directory
%if reReadTSet=0, it directly reads the matlab file TrainingSet.mat

%options for the curve fitting procedure
lsqoptions = optimoptions('lsqcurvefit','MaxFunctionEvaluations',2e5,'SpecifyObjectiveGradient',false,'FunctionTolerance',1e-10,'OptimalityTolerance',1e-10,'StepTolerance',1e-15,'Display','off');
%physical parameters to compute kon & koff from pon & poff
dt=1e-6;
Lnuc=6;
interaction_zone=0.01;
omega=5;

% kon=interaction_zone/Lnuc/dt*pon;
% koff=poff/dt;

%number of randomly-chosen simulations to show in a pannel
nperpannel=16;

%the directory that contains the training set data 
TrainDirName='data/TrainSamples/';
listing=dir(TrainDirName);
dirs=listing([listing.isdir]);
dirnames=char({dirs.name});
ndirnames=size(dirnames,1);
results=[];


%the nomenclature for directory names in the TestDirName directory must be:
%FCS_D2XX_D3YY where XX and YY are the values of D2D and D3D, respectively

if reReadTSet
    % 1-extract the database of tau_1/2 data from the dir TrainDirName
    % i.e. read the Goftau time series and fit them to get the corresponding 
    % tau_1/2
    wxyvec={};
    nexpe=1;
    nf=1;
    while nf<=ndirnames
        if strfind(dirnames(nf,:),'FCS')==1
            %by convention the directoy names is FCS_D2xx_D3yy
            currentdir=strtrim(dirnames(nf,:));
            %get the values of D2D and D3D in the currentdir directory
            D=ParseDirNameD2D3(currentdir);
            D2D=D(1);
            D3D=D(2);
            disp(dirnames(nf,:));
            currentdir=strtrim(dirnames(nf,:));%remove potential spaces
            Res_Dir=[TrainDirName,currentdir];
            listingfiles=dir(Res_Dir);
            filnames=char({listingfiles.name});
            %filnames contains the names of all the files from Res_Dir
            nw=1;
            %get pon and poff values for this directory
            pon=[];
            sim=1;
            while isempty(pon)
                fname=strtrim(filnames(sim,:));
                %by convention file names are pon_poff_wxy_autocorrelation.json
                fileok=strfind(fname,'0.');
                %a correct pon should be < 1
                if numel(fileok)>0 && fileok(1)==1
                    endc=strfind(fname,'_');
                    %pon is the value before the first underscore
                    pon=extract(fname,1,endc(1)-1);
                    %poff is the value between underscores 1 and 2
                    poff=extract(fname,endc(1)+1,endc(2)-1);
                end
                sim=sim+1;
            end
            %compute kon and koff based on pon and poff
            koff=poff/dt;
            kon=interaction_zone/Lnuc/dt*pon;
            %extracts the vector of wxy and the corresponding values of tau1/2
            [wxy,tauhalf]=AnalysisTrain([Res_Dir,'/'],[pon,poff,D2D,D3D],0);
            %save everything in results
            results(nexpe).koff=koff;
            results(nexpe).kon=kon;
            results(nexpe).poff=poff;
            results(nexpe).pon=pon;
            results(nexpe).D2D=D2D;
            results(nexpe).D3D=D3D;
            results(nexpe).wxy=wxy;
            results(nexpe).tauhalf=tauhalf;
            results(nexpe).KD=koff/kon;
            nexpe=nexpe+1;        
        end
        nf=nf+1;
    end
         %now that the raw data have been processeed save the in a single matlab
    %file (faster for the next runs)
    save(strcat(TrainDirName,'TrainingSet.mat'),'results');
    
else %read directly the database of tau_1/2 data from the dir TrainDirName
    load(strcat(TrainDirName,'TrainingSet.mat'));
end

%----- Fit the tau1/2 with the phenomenological diffusion laws with kon, koff, D2D and D3D knonw
%and alpha kappa2D kappa3D n1 n2 n3 as free parameters
% Here all the diffusion laws of the training sets ar fitted simultaneously
% with minimzation of the relative least square (see FitAll function below)
f=@(x)FitAll(x,results);
paramfit=[1 1 1 1 1 1];%[alpha kappa2D kappa3D n1 n2 n3]
[paramfit,fval]=fminsearchbnd(f,paramfit,zeros(1,6),[],optimset('PlotFcns',@optimplotfval,'MaxFunEvals',2e7));
%fminsearchbnd is a version of fminseach that accepts boundaries for the
%parameters, provided by woodchips@rochester.rr.com

%select a random sampel of diffusion laws for illustration, ie figure 1 b1 &
%b2
examples=randperm(size(results,2));
wtheo=logspace(-2,-0.18);
w2theo=wtheo.^2;
figure();
h=zeros(2*size(results,2));
for i=1:nperpannel
   k=examples(i); 
   wxy2=(results(k).wxy).^2;
   rcolor=rand(1,3);
   subplot(1,2,1), h(i)=plot(wxy2,results(k).tauhalf,'o','MarkerEdgeColor',rcolor,...
    'MarkerFaceColor',rcolor,'DisplayName',strcat('(',num2str(results(k).KD,'%.2f'),', ',num2str(results(k).D2D,'%.1f'), ', ',num2str(results(k).D3D,'%.0f'),')'));
   hold on; 
   subplot(1,2,1), plot(w2theo,Hybridtau(paramfit,results(k),wtheo),'-','color',rcolor,'HandleVisibility','off');
   k=examples(nperpannel+i); 
   wxy2=(results(k).wxy).^2;
   rcolor=rand(1,3); 
   subplot(1,2,2), plot(wxy2,results(k).tauhalf,'o','MarkerEdgeColor',rcolor,'MarkerFaceColor',rcolor,'DisplayName',strcat('(',num2str(results(k).KD,'%.2f'),', ',num2str(results(k).D2D,'%.1f'), ', ',num2str(results(k).D3D,'%.0f'),')'));
   hold on; 
   subplot(1,2,2), plot(w2theo,Hybridtau(paramfit,results(k),wtheo),'-','color',rcolor,'HandleVisibility','off');
end
subplot(1,2,1), 
ylabel('$\tau_{1/2}$ (s)','interpreter','latex'), xlabel('${w_{xy}}^2$ ($\mu m^2$)','interpreter','latex');
hold off
ylim auto
xlim([0 0.4]);
leg=legend('NumColumns',2);
title(leg,'$(K_D,\ D_{2D},\ D_{3D})$','interpreter','latex');
subplot(1,2,2), 
ylabel('$\tau_{1/2}$ (s)','interpreter','latex'), xlabel('${w_{xy}}^2$ ($\mu m^2$)','interpreter','latex');
hold off
ylim auto
xlim([0 0.4]);
leg=legend('NumColumns',2);
title(leg,'$(K_D,\ D_{2D},\ D_{3D})$','interpreter','latex');

sgtitle('Training phase','interpreter','latex');


%%% show final parameter values
disp('%% --- Fitted parameters ---%%');
disp(strcat('\alpha=',num2str(paramfit(1),'%.3f')));
disp(strcat('K_{2D}=',num2str(paramfit(2),'%.3f')));
disp(strcat('K_{3D}=',num2str(paramfit(3),'%.3f')));
disp(strcat('n_1=',num2str(paramfit(4),'%.3f')));
disp(strcat('n_2=',num2str(paramfit(5),'%.3f')));
disp(strcat('n_3=',num2str(paramfit(6),'%.3f')));
disp(strcat('Final fval=',num2str(fval,'%.3f')));

%Bagging procedure
Nbags=100;%number of bags / variants of the Trainig Set
paramfitbags=zeros(numel(paramfit),Nbags);%container for the parameters for each bag

for b=1:Nbags
    %generate bag # b
    %sample the Training set, with replacement
    bag=results(randi(numel(results),1,numel(results)));
    %fit bag #b
    g=@(x)FitAll(x,bag);
    pfit=[1 1 1 1 0.5 1];%[alpha K2D K3D n1 n2 n3]
    [pfit,fval]=fminsearchbnd(g,pfit,zeros(1,6),[100 10 500 5 5 5],optimset('PlotFcns',@optimplotfval,'MaxFunEvals',2e9,'MaxIter',1e4));
    paramfitbags(:,b)=pfit;
end

meanparamfitbags=mean(paramfitbags,2);
sdparamfitbags=std(paramfitbags,0,2);

%%% show parameter values from bagging
disp('%% --- mean parameters from bagging---%%');
disp(strcat('\alpha=',num2str(meanparamfitbags(1),'%.3f'),'+/-',num2str(sdparamfitbags(1),'%.3f')));
disp(strcat('K_{2D}=',num2str(meanparamfitbags(2),'%.3f'),'+/-',num2str(sdparamfitbags(2),'%.3f')));
disp(strcat('K_{3D}=',num2str(meanparamfitbags(3),'%.3f'),'+/-',num2str(sdparamfitbags(3),'%.3f')));
disp(strcat('n_1=',num2str(meanparamfitbags(4),'%.3f'),'+/-',num2str(sdparamfitbags(4),'%.3f')));
disp(strcat('n_2=',num2str(meanparamfitbags(5),'%.3f'),'+/-',num2str(sdparamfitbags(5),'%.3f')));
disp(strcat('n_3=',num2str(meanparamfitbags(6),'%.3f'),'+/-',num2str(sdparamfitbags(6),'%.3f')));
disp(strcat('Final fval=',num2str(fval,'%.3f')));

realKDs=[results.KD];
disp(['# samples= ',num2str(numel(results)),', # <=1 =',num2str(sum(realKDs<=1)),', frac >1 = ',num2str(sum(realKDs>1)/numel(results))]);



FitParam.alpha=paramfit(1);
FitParam.K2D=paramfit(2);
FitParam.K3D=paramfit(3);
FitParam.n1=paramfit(4);
FitParam.n2=paramfit(5);
FitParam.n3=paramfit(6);
FitParam.fval=fval;

save('TrainResParam.mat','meanparamfitbags');

% %% --- mean parameters from bagging---%%
% \alpha=1.162+/-0.110
% K_{2D}=0.723+/-0.103
% K_{3D}=16.844+/-4.175
% n_1=1.104+/-0.017
% n_2=0.202+/-0.031
% n_3=1.372+/-0.008
% # samples= 150, # <=1 =45, frac >1 = 0.7


function s=FitAll(x,res)
%the function that fits all the diffusion laws of the training set
%simultaneously by minimizing the relative square error ie
%(tauhalf_data - tauhalf_theoretical)/ tauhalf_data
s=0;
nexp=size(res,2);
for i=1:nexp
    %the phenomonological diffuson law with kon, koff, D2D and D3D known is
    %stored in Hybridtau below
    tautheo=Hybridtau(x,res(i));
    s= s+norm(([res(i).tauhalf]-tautheo')./[res(i).tauhalf])^2;
end

end

function tautheo=Hybridtau(x,currres,wextern)
%the phenomonological diffuson law with kon, koff, D2D and D3D known is
global omega
alpha=x(1);
kappa2D=x(2);
kappa3D=x(3);
n1=x(4);
n2=x(5);
n3=x(6);

switch nargin
    case 2
        wxy=currres.wxy;
    case 3
        wxy=wextern;
end

KD=currres.koff/currres.kon;
phi=alpha/(kappa2D+currres.D2D)*currres.D3D/(kappa3D+currres.D3D)*KD^n1*exp((n2*log(KD))^2);
tautheo=wxy.^2./(4*(1+phi*(omega*wxy).^n3)).*(1/currres.D2D+phi*(omega*wxy).^n3/currres.D3D);


end



function outv=ParseDirNameD2D3(currentdir)
%a parser that extract parameter values from the directory name
%TODO: factor out this function, used elsewhere, to avoid copy-pasting of
%code
D2D=0;
D3D=0;


%the nomenclature for directory names is:
%FCS_D2XXX_D3XXX

%read D2D
startc=strfind(currentdir,'D2');
endc=strfind(currentdir,'D3');
D2D=extract(currentdir,startc+2,endc-2);

%read D3D
startc=endc+2;
endc=numel(currentdir);
D3D=extract(currentdir,startc,endc);

outv=[D2D,D3D];

end

function ostr=ptodot(istr)
%by convention, the decimal point in the file name is replaced by a 'p'
%this function replace 'p''s in the file name by the original '.'
%TODO: factor out this function, used elsewhere, to avoid copy-pasting of
%code
ostr=istr;
if contains(istr,'p')
    ostr(strfind(istr,'p'))='.';
end
end

function onumber=extract(istr,begc,endc)
%extract the content of string istr between begc and endc
%TODO: factor out this function, used elsewhere, to avoid copy-pasting of
%code
ostr=istr(begc:endc);
ostr=ptodot(ostr);
onumber=str2double(ostr);
end
