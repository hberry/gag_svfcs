function [wxy,Fit_Res]=AnalysisTrain(Res_Dir,constants,plotGoftau)

% AnalysisTrain- This function extract the FWMH tau1/2 from the auto-correlation curves G(tau).
%
% 
% USAGE: [wxy,Fit_Res]=AnalysisTrain(Res_Dir,constants,plotGoftau)
%
% INPUT: Res_Dir: a string that gives the name of the directory that contains the training set data   
%        constants: a vector of 4 parameters that set the parameter to be analyzed
%           constants(1) : pon
%           constants(2) : poff
%           constants(3) : D2D
%           constants(4) : D3D
%       plotGoftau: if plotGoftau=1, G(\tau)=f(log(\tau) is plotted, for each of the simulation resuls files present in Res_Dir. 
%                   if plotGoftau is not 1, nothing is plotted
%
% OUTPUT: wxy      - a column vector giving the value of the synpatic weight at each integration step
%         Fit_Res  - a structure contaning the results of model integration, in particular with fields:
%                       - allsol.x: integration times
%                       - allsol.y: integration solution, ie allsol.y(1,:) gives the time evolution of neuronal Na
%                       - see matlab help on ODE15s for the other fields       
%        
% AUTHOR: Hugues Berry
%         hugues.berry@inria.fr
%         http://www.inrialpes.fr/Berry/
%
%REFERENCE: Mouttou et al. Quantifying 2022 membrane binding using Fluorescence Correlation Spectroscopy diffusion laws
%
% LICENSE: CC0 1.0 Universal





global dt Lnuc interaction_zone

%dt is the time step of the FCS simulator (in seconds)
%Lnuc is the linear size of the cubic nucleus (L in the article)
%interaction_zone is the distance below which a gag molecul can bind the
%membrane ($\epsilon$ un the article)



pon=constants(1);
poff=constants(2);
D2D=constants(3);
D3D=constants(4);
kon=interaction_zone/Lnuc/dt*pon;
koff=poff/dt;
KD=koff/kon;

format long g;
set(0,'defaultTextInterpreter','latex');

%options for the curve fitting procedure
lsqoptions = optimoptions('lsqcurvefit','MaxFunctionEvaluations',2e5,'SpecifyObjectiveGradient',false,'FunctionTolerance',1e-10,'OptimalityTolerance',1e-10,'StepTolerance',1e-15,'Display','off');
options=optimset('display','off','Tolfun',1e-9,'TolX',1e-9);


%%1/Delay vector, ie the values of \tau
%%WARNING the delay vector must be present in Res_Dir (produced by the
%%FCS simulator)
tau=readjson([Res_Dir,'log_time.json']);
if sum(tau)==0
    error('the delay file log_time.json is empty');
end

tau=tau*dt;


%parse wxy from file name and extract Gtau

%the nomenclature for file names is:
%pon_poff_wxy_autocorrelation.json

listingfiles=dir(Res_Dir);
filnames=char({listingfiles.name});
nfiles=1;
nok=1;

while nfiles<size(listingfiles,1)
    if min(strfind(filnames(nfiles,:),num2str(pon)))==1 & ~contains(filnames(nfiles,:),'boundfrac')
        s=strfind(filnames(nfiles,:),'_');
        wxy=str2double(filnames(nfiles,(s(2)+1):(s(3)-1)));
        if isnan(wxy)
            error(['error reading ',filnames(nfiles,:)]);
        end
        if ~isempty(wxy)
            wxyvec(nok)={num2str(wxy)};
            %disp(strcat(Res_Dir,strtrim(filnames(nfiles,:))));
            Gtau(:,nok)=readjson(strcat(Res_Dir,strtrim(filnames(nfiles,:))));
        end
        nok=nok+1;
    end
   nfiles=nfiles+1;
end

wxy=str2double(wxyvec);
nvol=numel(wxy);
Ginf=zeros(1,nvol);
tauD=zeros(1,nvol);


res=[];


%Fit the Gtau by G(0)/(1+tau/tauD)+G(inf) to find G(inf) - expected to be
%around 1
for i=1:nvol
    x0=[100 1 1];
    [xest,~,~,~,~] = lsqcurvefit(@Goftau_Fit3d,x0,tau,Gtau(:,i),[0 0 0],[500 10 2],lsqoptions);
    tauD(i)=xest(2);
    Ginf(i)=xest(3);
end



%Plot G(\tau)=f(log(\tau)) if demanded (ie if plotGoftau==1)
if plotGoftau==1
    figure();
    for i=1:nvol
        loglog(tau,Gtau(:,i),'color',rand(1,3),'LineWidth',1,'DisplayName',num2str(wxy(i)));
        hold on;
    end
    ylabel('$G(\tau)$'), xlabel('$\tau$ (s)');
    hold off
    ylim auto
    leg=legend('show','NumColumns',2);
    title(leg,'$w_{xy}$','interpreter','latex');
    title(['$k_\mathrm{on}=$',num2str(kon),', $k_\mathrm{off}=$',num2str(koff),' s$^{-1}$'],'interpreter','latex');
end


%--------- Get the tau1/2
Fit_Res=tauHalf(Gtau,tau,Ginf);


end

function outvec=readjson(filename)
%reads the json output files of the FCS simulator
outvec=zeros(64,1);
i=1;
fid = fopen(filename);
tline=fgetl(fid);
while ~strcmp(tline,'[')
    tline=fgetl(fid);
end

while ~strcmp(tline,']')
    tline=fgetl(fid);
    element=str2double(tline);
    if ~isempty(element)
        outvec(i)=element;
    end
    i=i+1;
end
outvec(isnan(outvec))=[];
fclose(fid);
end



function f = Goftau_Fit3d(x,tau)
%a simple Brownian 3D fit with Ginf as baseline
global omega
f=0;

barN=x(1);
tauD=x(2);
Ginf=x(3);

f=1./(barN*(1+tau/tauD).*sqrt(1+tau/(omega^2*tauD)))+Ginf;
end


function tau1o2=tauHalf(Gtau,tau,Ginf)
%extrapolates the value of tau that correspponds to the FWMH
nvol=size(Gtau,2);
tau1o2=zeros(nvol,1);


for i=1:nvol  
    G0o2=0.50*(Gtau(1,i)-Ginf(i))+Ginf(i);
    il=find(Gtau(:,i)<=G0o2,1,'first')-1;
    slope=(Gtau(il+1,i)-Gtau(il,i))/(tau(il+1)-tau(il));
    tau1o2(i)=(G0o2-Gtau(il,i))/slope+tau(il);
end



end




