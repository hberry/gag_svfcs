function F = MyFitTest(x,wxy,D3D)
% MyFitTest- This function defines the phenomenological diffusion law with fit parameters set by training
% and KD and D2D as unknown paramters.
%
% 
% USAGE: F = MyFitTest(x,wxy,D3D)
%
% INPUT: x: 	  - a vector of the unkown paramters:
%			x(1): KD
%			x(2): D2D   
%        wxy      - a vector giving the values of beam width used in the svFCS experiments analyzed
%        D3D: the value of D3D (considered known here)
%
% OUTPUT: F      - the value of the phenomenological diffusion law for each of the wxy, and given the parameters in x  
%        
% AUTHOR: Hugues Berry
%         hugues.berry@inria.fr
%         http://www.inrialpes.fr/Berry/
%
%REFERENCE: Mouttou et al. Quantifying 2022 membrane binding using Fluorescence Correlation Spectroscopy diffusion laws
%
% LICENSE: CC0 1.0 Universal

%the phenomenological diffusion law with fit parameters set by training
% and KD and D2D as unknown paramters
global omega
%Parameters to fit
KD=x(1);
D2D=x(2);

%Parameters from the training set obtained with relative loss
alpha=1.162;
K2D=0.723;
K3D=16.844;
n1=1.104;
n2=0.202;
n3=1.372;


phi=alpha/(K2D+D2D)*D3D/(K3D+D3D)*KD^n1*exp((n2*log(KD))^2);
F=wxy.^2./(4*(1+phi*(omega*wxy).^n3)).*(1/D2D+phi*(omega*wxy).^n3/D3D);

end

