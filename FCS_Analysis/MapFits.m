% MapFits.m - This script firs simulation results with adhoc autocorrelation functions :
% 3D Brownian single population, 3D Brownian 2 populations, anomalous diffusion (ie Fractional Brownian)
% i.e. computes the figures of Figure SI1.
%
%        
%AUTHOR: Hugues Berry
%         hugues.berry@inria.fr
%         http://www.inrialpes.fr/Berry/
%
%REFERENCE: Mouttou et al. Quantifying 2022 membrane binding using Fluorescence Correlation Spectroscopy diffusion laws
%
% LICENSE: CC0 1.0 Universal




close all;clear all;

addpath 'Fonctions';
global dt Lnuc interaction_zone omega

%dt is the time step of the FCS simulator (in seconds)
%Lnuc is the linear size of the cubic nucleus (L in the article)
%interaction_zone is the distance below which a gag molecul can bind the
%membrane ($\epsilon$ un the article)
%omega is the ratio of w_z to w_xy (called s in the article)

reRead=1;
%if reRead=1, the code reads the raw simulation resuls from the directory
%if reRead=0, it directly reads the matlab file alldata.mat

reCompute=1;
%if reCompute=1, the code computes the best fit for each diffusion lax 
%if reCompute=0, it directly reads the best fit from file alldata.m

%options for the curve fitting procedure
lsqoptions = optimoptions('lsqcurvefit','MaxFunctionEvaluations',2e5,'SpecifyObjectiveGradient',false,'FunctionTolerance',1e-10,'OptimalityTolerance',1e-10,'StepTolerance',1e-15,'Display','off');


%physical parameters to compute kon & koff from pon & poff
dt=1e-6;
Lnuc=6;
interaction_zone=0.01;
omega=1.5/0.35;

%minimal relative likelihood AICc difference to consider that model 1 is significantly better than model 2 
%i.e. evidence for AICc difference is strong if exp(1/2(AICc2-AICc1))>ThreshRelLikeAICc
ThreshRelLikeAICc=0.05;

% kon=interaction_zone/Lnuc/dt*pon;
% koff=poff/dt;

%the directory that contains the data 
DirName='data/MapSamplesDB1p7DF30/';


%%1/Delay vector
tau=readjson([DirName,'log_time.json']);
if sum(tau)==0
    error('the delay file log_time.json is empty');
end
tau=tau*dt;

%parse wxy from file name and extract Gtau

%the nomenclature for file names is:
%pon_poff_wxy_autocorrelation.json


if reRead
    % 1-extract the database of Goftau data from the dir DirName
    listingfiles=dir(DirName);
    filnames=char({listingfiles.name});
    nfiles=1;
    nok=1;
    
    while nfiles<size(listingfiles,1)
        currfile=filnames(nfiles,:);
        if ~isempty(str2num(currfile(1)))
            s=strfind(currfile,'_');
            disp(currfile(1:(s(2)-1)));
            %read pon
            endc=s(1);
            alldata(nok).pon=extract(currfile,1,endc-1);
            alldata(nok).kon=interaction_zone/Lnuc/dt*alldata(nok).pon;
            %read poff
            startc=endc+1;
            endc=s(2);
            alldata(nok).poff=extract(currfile,startc,endc-1);
            alldata(nok).koff=alldata(nok).poff/dt;
            %read wxy
            startc=endc+1;
            endc=s(3);
            alldata(nok).wxy=extract(currfile,startc,endc-1);
            
            %read the file itself
            alldata(nok).Gtau=readjson(strcat(DirName,strtrim(filnames(nfiles,:))));
            nok=nok+1;
        end
        nfiles=nfiles+1;
    end
    %now that the raw data have been processeed save the in a single matlab
    %file (faster for the next runs)
    save(strcat(DirName,'alldata.mat'),'alldata');
    
else %read directly the database from the dir DirName
   load(strcat(DirName,'alldata.mat'));
end

%for ploting of G of tau
tauscale=logspace(log10(min(tau)),log10(max(tau)));

if reCompute
    disp('%%----- model selection -----%%');
    % model selection with AICc & BIC
    models={'brown','anomalous','2pop'};
    %nota that the indices of the models in models are ranked by increasing
    %number of free parameters: 2 for Brown, 3 for anomalous, 4 for 2pop
    nexp=size(alldata,2);
    
    for i=1:nexp
        %FitAll ccomputes the AICc (and the BIC) for the 3 models 
        fitresults=FitAll(tau, alldata(i).Gtau,lsqoptions);
        AICc=[fitresults.brown.AICc,fitresults.anom.AICc,fitresults.twopop.AICc];
    
        BestM=0;
        disp(['experiment #',num2str(i),':']);
        % the best model is the one with smallest AICc
        [minAICc,bestmodel]=min(AICc);
        
        %Test if the difference in AICc is strong enough
        RelLike=exp((minAICc-AICc)/2);
        RelLike(bestmodel)=0;
        if max(RelLike)<ThreshRelLikeAICc
            %model #bestmodel is significantly better than the 2 others
            disp(['best model is ',models{bestmodel}]);
            BestM=bestmodel;
        else
            %at least one other model is as good as model #bestmodel
            Supra=RelLike>=ThreshRelLikeAICc;
            if sum(Supra)==1
                %exactly one model is as good
                bestmodel2=find(Supra);
                disp([models{bestmodel},' and ',models{bestmodel2},' are equally good']);
                %keep the model with the smaller # of parameters
                BestM=min([bestmodel,bestmodel2]);
            else
                %all the models are as good
                disp('all models fit equally well');
                %keep the model with smallest # of parameters
                BestM=1;
            end
        end
        %a 2pop model with a population less than 1% is considered a
        %Brownian model
        if BestM==3 && (fitresults.twopop.fitpar(4)<0.01 ||fitresults.twopop.fitpar(4)>0.99)
           BestM=1; 
        end
        
        % recrord the best fit for the experiments
        alldata(i).bestfit=BestM-1;
        alldata(i).AICc=AICc;
    end
    save(strcat(DirName,'alldata.mat'),'alldata');
end

%draw the colormap fig SI1b1
%koff values
X=sort(unique([alldata.koff]));
%kon values
Y=sort(unique([alldata.kon]));
%the 2 matrix
Z=zeros(numel(X),numel(Y));

for i=1:numel(X)
    idxi=[alldata.koff]==X(i);
    for j=1:numel(Y)
        idxj=[alldata.kon]==Y(j);
        k=find(idxi.*idxj==1);
        Z(i,j)=alldata(k).bestfit;
    end
end
% the colormap
copp3=copper(3);

%plot using sanePColor, provided by jeremy@dartmouth.edu
subplot(4,1,[1 2]),sanePColor(log10(X),log10(Y),Z');

ylabel('$\log_{10} k_\mathrm{on}$ (s$^{-1}$)','interpreter','latex'), xlabel('$\log_{10} k_\mathrm{off}$ (s$^{-1}$)','interpreter','latex');
colormap(copp3);
c=colorbar('northoutside');
c.Ticks=[0.3,1,1.7];
c.TickLabels={'Brown','Anom','2pop'};
xlim([-1.1, 2.6]);

idxselec=[226, 144, 3];
xDsBrown=[];
xDsAnom=[];
xDs2pop=[];
DsBrown=[];
DsAnom=[];
Ds2pop=[];
rho=[];
%plot the estimates for Ds at kon=0.2883 s-1
idxi=abs([alldata.kon]-0.2883)<0.01;
for j=1:numel(X)
    idxj=[alldata.koff]==X(j);
    k=find(idxi.*idxj==1);
    fitresults=FitAll(tau, alldata(k).Gtau,lsqoptions);
    switch alldata(k).bestfit
        case 0
            xDsBrown=[xDsBrown X(j)];
            %D = w_xy^2/(4*tauD)
            DsBrown=[DsBrown 0.379^2/4./fitresults.brown.fitpar(2)];
        case 1
            xDsAnom=[xDsAnom X(j)];
            %D = w_xy^2/(4*tauD)
            DsAnom=[DsAnom 0.379^2/4./fitresults.anom.fitpar(2)];
        case 2
            xDs2pop=[xDs2pop X(j)];
            %D = w_xy^2/(4*tauD)
            Ds2pop=[Ds2pop 0.379^2/4./[fitresults.twopop.fitpar(2);fitresults.twopop.fitpar(3)]];
            rho=[rho fitresults.twopop.fitpar(4)];
    end
end

%plot figSI1 b2
subplot(4,1,3), plot(log10(xDsBrown),DsBrown,'o','MarkerEdgeColor',copp3(1,:),...
    'MarkerFaceColor',copp3(1,:),'Color',copp3(1,:));
hold on;
subplot(4,1,3), plot(log10(xDsAnom),DsAnom,'o','MarkerEdgeColor',copp3(2,:),...
    'MarkerFaceColor',copp3(2,:),'Color',copp3(2,:));
hold on;
subplot(4,1,3), plot(log10(xDs2pop),Ds2pop(1,:),'o','MarkerEdgeColor',copp3(3,:),...
    'MarkerFaceColor',copp3(3,:),'Color',copp3(3,:));
hold on;
subplot(4,1,3), plot(log10(xDs2pop),Ds2pop(2,:),'o','MarkerEdgeColor',copp3(3,:),...
    'MarkerFaceColor',copp3(3,:),'Color',copp3(3,:));
xlim([-1.1, 2.6]);
ylabel('$D_\mathrm{est}(k_\mathrm{on}= 0.29)$','interpreter','latex'), xlabel('$\log_{10} k_\mathrm{off}$ (s$^{-1}$)','interpreter','latex');
%plot figSI1 b3
subplot(4,1,4), plot(log10(xDs2pop),log10(rho./(1-rho)),'o','MarkerEdgeColor',copp3(3,:),...
    'MarkerFaceColor',copp3(3,:),'DisplayName','$K_{D,\mathrm{est}}$');
hold on
xDS2popscale=logspace(log10(min(xDs2pop)),log10(max(xDs2pop)));
subplot(4,1,4), plot(log10(xDS2popscale),log10(xDS2popscale/0.2883),'k--','DisplayName','$K_{D,\mathrm{real}}$');
xlim([-1.1, 2.6]);
xlabel('$\log_{10} k_\mathrm{off}$ (s$^{-1}$)','interpreter','latex');
ylabel('$\log_{10} K_D$','interpreter','latex');
legend('show','interpreter','latex');

%plot exemples of fit ie %plot figSI1a1, a2 and a3
figure();

for k=1:3
    i=idxselec(k);
    fitresults=FitAll(tau, alldata(i).Gtau,lsqoptions);
    %plots
    rcolor=rand(1,3);
    subplot(1,3,k), semilogx(tau,alldata(i).Gtau,'o','MarkerEdgeColor',rcolor,...
    'MarkerFaceColor',rcolor,'DisplayName',strcat('(',num2str(alldata(i).koff,'%.2f'),', ',num2str(alldata(i).kon,'%.2f'),')'));
    hold on;
    subplot(1,3,k), semilogx(tauscale,Goftau_3d(fitresults.brown.fitpar,tauscale),'r-','LineWidth',2,'DisplayName','Brown');
    subplot(1,3,k), semilogx(tauscale,Goftau_anomal(fitresults.anom.fitpar,tauscale),'k--','LineWidth',2,'DisplayName','Anom');
    subplot(1,3,k), semilogx(tauscale,Goftau_2pop(fitresults.twopop.fitpar,tauscale),'b:','LineWidth',2,'DisplayName','2pop');
    disp(['%% ---- Fit Results --- (koff,kon)=(', num2str(alldata(i).koff,'%.2f'),', ',num2str(alldata(i).kon,'%.2f'),')----%%']);
    disp(['1. Fit Brownien: param=',num2str(fitresults.brown.fitpar),', AICc=',num2str(fitresults.brown.AICc)]);
    disp(['2. Fit Anom: param=',num2str(fitresults.anom.fitpar),', AICc=',num2str(fitresults.anom.AICc)]);
    disp(['3. Fit 2pop: param=',num2str(fitresults.twopop.fitpar),', AICc=',num2str(fitresults.twopop.AICc)]);
    legend('show');
end



function outvec=readjson(filename)
%reads the json output files of the FCS simulator
outvec=zeros(64,1);
i=1;
fid = fopen(filename);
tline=fgetl(fid);
while ~strcmp(tline,'[')
    tline=fgetl(fid);
end

while ~strcmp(tline,']')
    tline=fgetl(fid);
    element=str2double(tline);
    if ~isempty(element)
        outvec(i)=element;
    end
    i=i+1;
end
outvec(isnan(outvec))=[];
fclose(fid);
end

function ostr=ptodot(istr)
%by convention, the decimal point in the file name is replaced by a 'p'
%this function replace 'p''s in the file name by the original '.'
%TODO: factor out this function, used elsewhere, to avoid copy-pasting of
%code
ostr=istr;
if contains(istr,'p')
    ostr(strfind(istr,'p'))='.';
end
end

function onumber=extract(istr,begc,endc)
%extract the content of string istr between begc and endc
%TODO: factor out this function, used elsewhere, to avoid copy-pasting of
%code
ostr=istr(begc:endc);
ostr=ptodot(ostr);
onumber=str2double(ostr);
end

function f = Goftau_3d(x,tau)
%Autocorrelation function of a 3D Brownian motion
global omega
f=0;

barN=x(1);%1/G(0)
tauD=x(2);%diffusion time tauD=w_xy^2/(4D)

f=1./(barN*(1+tau/tauD).*sqrt(1+tau/(omega^2*tauD)))+1;
end



function f = Goftau_anomal(x,tau)
%Autocorrelation function of an anomalous diffusion (Fractional Brownian)
f=0;

barN=x(1);%1/G(O)
tauD=x(2);%diffusion time tauD=w_xy^2/(4D)
alpha=x(3);%anomamous exponent

f=1./(barN*(1+(tau/tauD).^alpha))+1;
end


function f = Goftau_2pop(x,tau)
%Autocorrelation function of mixture of 2 3D Brownian motions with
%diffusion coefficient D1 and D2
global omega
f=0;

barN=x(1);%1/G(O)
tauD1=x(2);%diffusion time tauD=w_xy^2/(4D1)
tauD2=x(3);%diffusion time tauD=w_xy^2/(4D2)
rho=x(4);%ratio of the two popilations rho=C1/(C1+C2)=K_D/(1+K_D) 

f=1/barN*(rho./((1+tau/tauD1).*sqrt(1+tau/(omega^2*tauD1)))+(1-rho)./((1+tau/tauD2).*sqrt(1+tau/(omega^2*tauD2))))+1;
end

function  [AICc,BIC]=AICcBIC(y,fofx,k)
    %computes the AICc and BIC with datapoints y and prediction value fofx
    %k = # of parameters for the fit
    n=numel(y);%# of datapoints
    RSS=sum((y-fofx).^2);%residual sum of squares  
    AICc=2*k+n*log(RSS)+(2*k^2+2*k)/(n-k-1);
    BIC=k*log(n)+n*log(RSS);
end

function results=FitAll(tau, Gtau,lsqoptions)

%1-Classical Brownian 3D Fit
x1=[10 1];
[xest1,~,~,~,~] = lsqcurvefit(@Goftau_3d,x1,tau,Gtau,[0 0],[100 10],lsqoptions);
[AICc(1),BIC(1)]=AICcBIC(Gtau,Goftau_3d(xest1,tau),numel(x1));

%2-Anomalous Fit
x2=[10 1 1];
[xest2,~,~,~,~] = lsqcurvefit(@Goftau_anomal,x2,tau,Gtau,[0 0 0],[100 10 1],lsqoptions);
[AICc(2),BIC(2)]=AICcBIC(Gtau,Goftau_anomal(xest2,tau),numel(x2));

%3-2-population Fit
x3=[10 1 0.1 0.5];
[xest3,~,~,~,~] = lsqcurvefit(@Goftau_2pop,x3,tau,Gtau,[0 0 0 0],[100 10 10 1],lsqoptions);
[AICc(3),BIC(3)]=AICcBIC(Gtau,Goftau_2pop(xest3,tau),numel(x3));


results.brown.AICc=AICc(1);
results.brown.BIC=BIC(1);
results.brown.fitpar=xest1;

results.anom.AICc=AICc(2);
results.anom.BIC=BIC(2);
results.anom.fitpar=xest2;

results.twopop.AICc=AICc(3);
results.twopop.BIC=BIC(3);
results.twopop.fitpar=xest3;

end