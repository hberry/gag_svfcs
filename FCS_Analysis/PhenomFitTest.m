
% PhenomFitTest.m - This script fits phenomenological diffusion law on the test set 
% with kon,koff, D2D unknown 
% and all the parameters of the diffusion law fixed by the training 
% ie generate figure 1c1, 1c2 and 1c3 of the article
%
%        
%AUTHOR: Hugues Berry
%         hugues.berry@inria.fr
%         http://www.inrialpes.fr/Berry/
%
%REFERENCE: Mouttou et al. Quantifying 2022 membrane binding using Fluorescence Correlation Spectroscopy diffusion laws
%
% LICENSE: CC0 1.0 Universal

close all;clear all;
addpath 'Fonctions';

global dt Lnuc interaction_zone omega

set(0,'defaultTextInterpreter','latex');

%dt is the time step of the FCS simulator (in seconds)
%Lnuc is the linear size of the cubic nucleus (L in the article)
%interaction_zone is the distance below which a gag molecul can bind the
%membrane ($\epsilon$ un the article)
%omega is the ratio of w_z to w_xy (called s in the article)


reReadTSet=1;
%if reReadTSet=1, the code reads the raw simulation resuls from the directory
%if reReadTSet=0, it directly reads the matlab file TestSet.mat

%options for the curve fitting procedure
lsqoptions = optimoptions('lsqcurvefit','MaxFunctionEvaluations',2e5,'SpecifyObjectiveGradient',false,'FunctionTolerance',1e-10,'OptimalityTolerance',1e-10,'StepTolerance',1e-15,'Display','off');

%physical parameters to compute kon & koff from pon & poff
dt=1e-6;
Lnuc=6;
interaction_zone=0.01;
omega=5;

% kon=interaction_zone/Lnuc/dt*pon;
% koff=poff/dt;

%number of randomly-chosen simulations to show in the pannel
nperpannel=11;  

%the directory that contains the test set data 
TestDirName='data/TestSamples/';

listing=dir(TestDirName);
dirs=listing([listing.isdir]);
dirnames=char({dirs.name});
ndirnames=size(dirnames,1);
results=[];


%the nomenclature for directory names in the TestDirName directory must be:
%FCS_D2XX_D3YY where XX and YY are the values of D2D and D3D, respectively

if reReadTSet
    % 1-extract the database of tau_1/2 data from the dir TestDirName
    % i.e. read the Goftau time series and fit them to get the corresponding 
    % tau_1/2
    wxyvec={};
    nexpe=1;
    nf=1;
    while nf<=ndirnames
        if strfind(dirnames(nf,:),'FCS')==1
            %by convention the directoy names is FCS_D2xx_D3yy
            currentdir=strtrim(dirnames(nf,:));
            %get the values of D2D and D3D in the currentdir directory
            D=ParseDirNameD2D3(currentdir);
            D2D=D(1);
            D3D=D(2);
            disp(dirnames(nf,:));
            currentdir=strtrim(dirnames(nf,:));%remove potential spaces
            Res_Dir=[TestDirName,currentdir];
            listingfiles=dir(Res_Dir);
            filnames=char({listingfiles.name});
            %filnames contains the names of all the files from Res_Dir
            nw=1;
            %get pon and poff values for this directory
            pon=[];
            sim=1;
            while isempty(pon)
                fname=strtrim(filnames(sim,:));
                %by convention file names are pon_poff_wxy_autocorrelation.json
                fileok=strfind(fname,'0.');
                %a correct pon should be < 1
                if numel(fileok)>0 && fileok(1)==1
                    endc=strfind(fname,'_');
                    %pon is the value before the first underscore
                    pon=extract(fname,1,endc(1)-1);
                    %poff is the value between underscores 1 and 2
                    poff=extract(fname,endc(1)+1,endc(2)-1);
                end
                sim=sim+1;
            end
            %compute kon and koff based on pon and poff
            koff=poff/dt;
            kon=interaction_zone/Lnuc/dt*pon;
            %extracts the vector of wxy and the corresponding values of tau1/2
            [wxy,tauhalf]=AnalysisTrain([Res_Dir,'/'],[pon,poff,D2D,D3D],0);
            %save everything in results
            results(nexpe).koff=koff;
            results(nexpe).kon=kon;
            results(nexpe).poff=poff;
            results(nexpe).pon=pon;
            results(nexpe).D2D=D2D;
            results(nexpe).D3D=D3D;
            results(nexpe).wxy=wxy;
            results(nexpe).tauhalf=tauhalf;
            results(nexpe).KD=koff/kon;
            nexpe=nexpe+1;
        end
        nf=nf+1;
    end
     %now that the raw data have been processeed save the in a single matlab
    %file (faster for the next runs)
    save(strcat(TestDirName,'TestSet.mat'),'results');
    
else %read directly the database of tau_1/2 data from the dir TestDirName
    load(strcat(TestDirName,'TestSet.mat'));
end


%----- Fit the tau1/2 with the Hybrid 3D-2D expression and paramters from
%training
nexp=size(results,2);
x0=[1 1];
for i=1:nexp
    %the phenomenological diffusion law with unknown D2D, kon and koff
    %and fixed parameters is in MyFitTest.m
    % fit is done experiment by experiment
    f=@(x,y)MyFitTest(x,y,results(i).D3D);
    [xest,~,~,~,~] = lsqcurvefit(f,x0,results(i).wxy,(results(i).tauhalf)',[0 0],[500 20],lsqoptions);
    results(i).FitParam.KD=xest(1);
    results(i).FitParam.D2D=xest(2);  
end

%select a random sample of diffusion laws for plotting as illustration, ie figure 1 c1
examples=randperm(size(results,2));
wtheo=logspace(-1.2,-0.19);
w2theo=wtheo.^2;
figure();
h=zeros(2*size(results,2));
for i=1:nperpannel
   k=examples(i);  
   wxy2=(results(k).wxy).^2;
   rcolor=rand(1,3);
   subplot(2,3,[1 2 4 5]),h(i)=plot(wxy2,results(k).tauhalf,'o','MarkerEdgeColor',rcolor,...
    'MarkerFaceColor',rcolor,'DisplayName',strcat('(',num2str(results(k).KD,'%.2f'),', ',num2str(results(k).D2D,'%.1f'),', ',num2str(results(k).D3D,'%.0f'),')'));
   hold on; 
   subplot(2,3,[1 2 4 5]), plot(w2theo,MyFitTest([results(k).FitParam.KD,results(k).FitParam.D2D],wtheo,results(k).D3D),'-','color',rcolor,'HandleVisibility','off');
end
ylabel('$\tau_{1/2}$ (s)'), xlabel('${w_{xy}}^2$ ($\mu m^2$)');
hold off
ylim auto
xlim([0 0.4]);
leg=legend('NumColumns',2);
title(leg,'$(K_\mathrm{D},\ D_{2D},\ D_{3D})$','interpreter','latex');


%compare the estimations for KD and D2D to the real values
% ie figure 1c2 and c3
realKDs=[results.KD];
realD2Ds=[results.D2D];
predKDs=zeros(1,nexp);
predD2Ds=zeros(1,nexp);

for i=1:nexp
    predKDs(i)=results(i).FitParam.KD;
    predD2Ds(i)=results(i).FitParam.D2D;
end

subplot(2,3,3) , plot(realKDs,predKDs,'o','MarkerEdgeColor','r',...
    'MarkerFaceColor','r');ylabel('a');
    KDfit=linspace(0.1,10);
    hold on,subplot(2,3,3), plot(KDfit,KDfit,'-'); 
    %ylim([-4 2.5]);xlim([-2 3.5]);
    ylabel('predicted $K_D$'), xlabel('real $K_D$');
%    title(['$a=',num2str(prefKD),'\times K_D^{',num2str(expoKD),'}$'],'interpreter','latex');

subplot(2,3,6), plot(realD2Ds,predD2Ds,'o','MarkerEdgeColor','b',...
    'MarkerFaceColor','b');
    D2Dfit=linspace(1.2,5.5);
    hold on,subplot(2,3,6), plot(D2Dfit,D2Dfit,'-');
    %ylim([1 2]);xlim([0 20]);
    ylabel('predicted $D_{2D}$'), xlabel('real $D_{2D}$');
%    title(['$\langle n \rangle= ',num2str(mean(Pn)),'\pm',num2str(std(Pn)),'$'],'interpreter','latex');

errKD=(exp(median(abs(log(predKDs./realKDs))))-1)*100;
errD2D=(exp(median(abs(log(predD2Ds./realD2Ds))))-1)*100;

subplot(2,3,[1 2 4 5]), title('Test phase');
subplot(2,3,3), title(['$\epsilon(K_D)=',num2str(errKD,'%.2f'),'\%$'],'interpreter','latex');
subplot(2,3,6), title(['$\epsilon(D_{2D})=',num2str(errD2D,'%.2f'),'\%$'],'interpreter','latex');

realKDs=[results.KD];
disp(['# samples= ',num2str(numel(results)),', frac >1 = ',num2str(sum(realKDs>1)/numel(results))]);



function outv=ParseDirNameD2D3(currentdir)
%a parser that extract parameter values from the directory name
%TODO: factor out this function, used elsewhere, to avoid copy-pasting of
%code

%the nomenclature for directory names is:
%FCS_D2XXX_D3XXX

D2D=0;
D3D=0;


%read D2D
startc=strfind(currentdir,'D2');
endc=strfind(currentdir,'D3');
D2D=extract(currentdir,startc+2,endc-2);

%read D3D
startc=endc+2;
endc=numel(currentdir);
D3D=extract(currentdir,startc,endc);

outv=[D2D,D3D];


end

function ostr=ptodot(istr)
%by convention, the decimal point in the file name is replaced by a 'p'
%this function replace 'p''s in the file name by the original '.'
%TODO: factor out this function, used elsewhere, to avoid copy-pasting of
%code
ostr=istr;
if contains(istr,'p')
    ostr(strfind(istr,'p'))='.';
end
end

function onumber=extract(istr,begc,endc)
%extract the content of string istr between begc and endc
%TODO: factor out this function, used elsewhere, to avoid copy-pasting of
%code
ostr=istr(begc:endc);
ostr=ptodot(ostr);
onumber=str2double(ostr);
end
