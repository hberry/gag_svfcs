
/* The particle class ie the class for HIV-1 Gag proteins in the code
*/
/* The particle class ie the class for HIV-1 Gag proteins in the code

# AUTHOR: Hugues Berry & Barthélémy Caron
#         hugues.berry@inria.fr
#         http://www.inrialpes.fr/Berry/
#
# REFERENCE: Mouttou et al. Quantifying membrane binding using Fluorescence Correlation Spectroscopy diffusion laws
#
# LICENSE: CC0 1.0 Universal
*/

#include <vector>

#ifndef DEF_PARTICLE
#define DEF_PARTICLE

using std::vector;

class Particle
{
    public:
     
    //Methods
    Particle(int index,
			 float pos1,
    		 float pos2,
    		 float pos3,
    		 float diff2d,
    		 float diff3d,
    		 float pbind,
    		 float punbind,
    		 float hei,
    		 float wxy_main,
    		 float IZ,
    		 float vol,
    		 float brill);
    void displacement(float rd1, float rd2, float rd3);
    void temp_displacement(float rd1, float rd2, float rd3);
    int compute_cell();
    void check_binding(const float unif01);
    int is_bound();
    float photon_emission();
    float* printpos();
    float* printpos2();
    float* print_temp_pos();
     
     
    //Attributs
     int index;
    float position[3];
    float initial_position[3];
    float relative_position[3];
    float temp_position[3];
    float diff_2d;
    float diff_3d;
    bool binding_state;
    float p_binding;
    float p_unbinding;
    float wz;
    float wxy;
    float brillance;
    float width_interaction_zone;
    float volume;
    float half_width;
    std::vector<int> is_bound_with;
};
 
#endif
