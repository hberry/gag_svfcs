/* The particle class ie the class for HIV-1 Gag proteins in the code

# AUTHOR: Hugues Berry & Barthélémy Caron
#         hugues.berry@inria.fr
#         http://www.inrialpes.fr/Berry/
#
# REFERENCE: Mouttou et al. Quantifying membrane binding using Fluorescence Correlation Spectroscopy diffusion laws
#
# LICENSE: CC0 1.0 Universal
*/


#include "Particle.h"
#include <cstdlib>
#include <math.h>
#include <iostream>
#include <vector>

// Here is defined the Particle class necessary to the gag_sim simulations


// Particle constructor
Particle::Particle( int i,
					float pos1,
					float pos2,
					float pos3,
					float diff2d,
					float diff3d,
					float pbind,
					float punbind,
					float hei,
					float wxy_main,
					float IZ,
					float vol,
					float brill) :
					index(i),
					diff_2d(diff2d),
					diff_3d(diff3d),
					p_binding(pbind),
					p_unbinding(punbind),
					wz(hei),
					wxy(wxy_main),
					width_interaction_zone(IZ),
					volume(vol),
					brillance(brill)
{
    position[0] = pos1;
    position[1] = pos2;
    position[2] = pos3;
    initial_position[0] = pos1;
    initial_position[1] = pos2;
    initial_position[2] = pos3;
    relative_position[0] = 0;
    relative_position[1] = 0;
    relative_position[2] = 0;
    temp_position[0] = 0;
    temp_position[1] = 0;
    temp_position[2] = 0;
    binding_state = false;
    half_width = volume / 2;
}



//this method temporary move the particle
void Particle::temp_displacement(float rd1, float rd2, float rd3) {
    temp_position[1] = position[1] + diff_3d * rd2;
    temp_position[2] = position[2] + diff_3d * rd3;
}	

//this method move the particle
void Particle::displacement(float rd1, float rd2, float rd3) {
    if (binding_state == true) {
        position[0] = - half_width + 0.5*width_interaction_zone;
        position[1] += diff_2d * rd2;
        position[2] += diff_2d * rd3;
    }
    
    else {
        position[0] += diff_3d * rd1;
        position[1] += diff_3d * rd2;
        position[2] += diff_3d * rd3;
    }
}



// check if an (un)binding (from)to the membrane is possible and apply it if the probability allows it
void Particle::check_binding(const float unif01) {
    if (binding_state == true & is_bound_with.size() == 0) {
        if ( unif01 < p_unbinding) {
			binding_state = false;
		}
    }

    else {
        if (position[0] <= (width_interaction_zone - half_width)) {
            if (unif01 < p_binding) {
				binding_state = true;
			}
        }
    }
}

// return the binding state of the particle as an integer: 
// 0 if the particle is not bound to the membrane
// 1 if the particle is bound to the membrane
// 2 if the particle is bound to the membrane AND to another gag
int Particle::is_bound() {
	if (binding_state == true) {
		if (is_bound_with.size() == 0)  {
			return 1;
		}
		else {
			return 2;
		}
	}

	else {
		return 0;
	}
}

//compute the illumination in function of the position of the particle
float Particle::photon_emission() {
	//if the psf is centered in z on the bottom ie the membrane at z=-half_width
 	return brillance*exp(- ( 2 * (pow(position[1], 2) + pow(position[2], 2)) / pow(wxy, 2)) - ((2 * pow(position[0] + half_width, 2)) / pow(wz, 2)));
 	//if the psf is centered in z on the middle of the volume ie at z=0
 	//return brillance*exp(- ( 2 * (pow(position[1], 2) + pow(position[2], 2)) / pow(wxy, 2)) - ((2 * pow(position[0], 2)) / pow(wz, 2)));
}

//return the last computed position of the particle
float* Particle::printpos() {
    return position;
}

//return the relative position of a particle from its origin. if a rebound alter the position of the particle, its initial position is 
// altered also. This method is used to compute the MSD
float* Particle::printpos2() {
    relative_position[0] = position[0] - initial_position[0];
    relative_position[1] = position[1] - initial_position[1];
    relative_position[2] = position[2] - initial_position[2];
    return relative_position;
}

// return the temporary position of the particle
float* Particle::print_temp_pos() {
	return temp_position;
}
